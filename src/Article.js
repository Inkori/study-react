import React, {Component} from 'react'
import CommentList from './CommentList'

export default class Article extends Component {

	constructor(props) {
		super(props);

		this.state = {
			isOpen: false
		}
		// this.toggleOpen = this.toggleOpen.bind(this)
	}
	// Либо баинд либо стрелочная функция


	render() {
		const {article} = this.props
		const {isOpen} = this.state
		// const body = isOpen ? <section>{article.text}</section> : null
		return(
					<div>
						<h3>{article.title}</h3>
						<button onClick = {this.toggleOpen}>
							{isOpen ? 'close' : 'open'}
						</button>
						{this.getBody()}
					</div>
		)
	}



getBody() {
	if(!this.state.isOpen) return null
	const {article} = this.props
	return <section>{article.text} <CommentList comments={article.comments} /></section>
}
toggleOpen = () => {
	this.setState({
		isOpen: !this.state.isOpen
	})
}
}


// class Article extends React.Component{
// 	render() {

// 		return (
// 			<div>
// 				<h3>title</h3>
// 				<section>Body</section>
// 			</div>
// 		)
// 	}


// }

// export default function Article(props) {
// 	const {article} = props
// 	return (
// 		<div>
// 			<h3>{article.title}</h3>
// 			<section>{article.text}</section>
// 		</div>
// 	)
// }

// export default Article;