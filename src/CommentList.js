import React from 'react'
import Comment from './Comments'

export default class  CommentList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
        isOpen: true
    };
}






    render() {
      if(!this.props.comments || this.props.comments.length == 0) return <h4>There is no comments for this article</h4>;

    return(

      <div>

        <button onClick = {this.hendleClick}>
							{this.comBody()}
						</button>
						{this.getComments()}
      </div>

    )
  };



  getComments() {
    if(this.state.isOpen) return null
    const commentEl = this.props.comments.map(comment =>
          <li key = {comment.id}>
            <Comment comment={comment}/>
          </li>
     )
     return (
       <div>
         <h4>Comments {this.props.comments.length}</h4>
         <ul>{commentEl}</ul>
       </div>
     )
    return <section>{comment.text}</section>
  };
  hendleClick = (ev) => {
    ev.preventDefault();
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  comBody() {
    return this.state.isOpen ? "hide comments" : "show comments"
  }

}