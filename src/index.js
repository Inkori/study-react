import React from 'react'
import {render} from 'react-dom'

import Article from './article'
import ArticleList from './ArticleList'
// import CommentList from './CommentList'
// import Comments from './Comments'
import {articles} from './fixtures'



class App extends React.Component{
	render() {
		return(
			<main>
				<ArticleList articles={articles} />
			</main>
		)
	}
}

render(<App />, document.getElementById("container"))